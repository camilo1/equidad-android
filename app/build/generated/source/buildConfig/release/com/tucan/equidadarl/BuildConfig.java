/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.tucan.equidadarl;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.tucan.equidadarl";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0";
}
