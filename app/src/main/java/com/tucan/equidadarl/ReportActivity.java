package com.tucan.equidadarl;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ReportActivity extends Activity {

	Report_CustomList CustomList;
	private int index;
	boolean isAutoScrolling = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		Intent intent1 = getIntent();
		final String userName = intent1.getStringExtra("UserName");
		final String owner = intent1.getStringExtra("owner");
		ImageButton home_btn = (ImageButton) findViewById(R.id.home2);
		ImageButton webLink1 = (ImageButton) findViewById(R.id.webLink2);
		webLink1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ReportActivity.this,
						WebPageActivity.class);
				startActivity(intent);
			}

		});
		home_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}

		});

		ParseQuery<ParseObject> query = ParseQuery.getQuery("ImageData");
		query.whereEqualTo("ownername", owner);
		query.orderByDescending("date");

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(final List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					final ListView list = (ListView) findViewById(R.id.list);
					if (objects.isEmpty() == true) {
						list.setVisibility(View.GONE);
						TextView noData = (TextView) findViewById(R.id.noData);
						noData.setVisibility(View.VISIBLE);
					} else {
						list.setVisibility(View.VISIBLE);
						CustomList = new Report_CustomList(ReportActivity.this,
								0, 0, objects);

						list.setAdapter(CustomList);

						/*
						 * list.setOnScrollListener(new OnScrollListener() {
						 * 
						 * @Override public void
						 * onScrollStateChanged(AbsListView view, int
						 * scrollState) { // TODO Auto-generated method stub if
						 * (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
						 * if (isAutoScrolling) return; isAutoScrolling = true;
						 * int pos = list.getFirstVisiblePosition();
						 * list.setSelection(pos); isAutoScrolling = false; } }
						 * 
						 * @Override public void onScroll(AbsListView view, int
						 * firstVisibleItem, int visibleItemCount, int
						 * totalItemCount) { // TODO Auto-generated method stub
						 * }
						 * 
						 * });
						 */
						ImageButton downBtn = (ImageButton) findViewById(R.id.downBtn);
						downBtn.setOnClickListener(new OnClickListener() {

							@SuppressLint("NewApi")
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								index = list.getFirstVisiblePosition();
								index = index + 1;
								list.setSelection(index);
							}
						});

					}
				} else {
					ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					final NetworkInfo ni = cm.getActiveNetworkInfo();
					if (ni == null) {
						// There are no active networks.
						Toast.makeText(getBaseContext(), "Red no disponible",
								Toast.LENGTH_SHORT).show();
					}
					e.printStackTrace();
				}
			}
		});

	}
}
