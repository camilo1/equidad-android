package com.tucan.equidadarl;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

public class WebPageActivity extends Activity {

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_page);
		WebView webLink = (WebView) findViewById(R.id.webLink);
		webLink.getSettings().setJavaScriptEnabled(true);
		webLink.getSettings().setBuiltInZoomControls(true);

		final Activity activity = this;
		webLink.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(activity, description, Toast.LENGTH_SHORT)
						.show();
			}
		});
		ImageButton bck = (ImageButton) findViewById(R.id.bckBtn6);
		// bck.setText("<");
		webLink.loadUrl("http://www.laequidadseguros.coop");
		findViewById(R.id.bckBtn6).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
}