package com.tucan.equidadarl;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings.Secure;
import android.util.Log;

import com.meteosoft.infrastructure.Utils;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.splunk.mint.Mint;

public class WayGroup extends Application {
	LocationManager lm;
	LocationListener ll;
	static String location = " ";
	static SharedPreferences preference;
	String MyLocation = "MyLocation";
	String badgeNo = "badgeNo";

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		Mint.initAndStartSession(WayGroup.this, "4902020a");
		preference = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
		String ApplicationId = "CpkjkPtVi84QARtGotGpJ99Q1XnJgJsvjfEfsqsK";
		String ClientKey = "QTrhSF0HDPPTfw9TMR22oBfmJa1i4jyzFBPi1cGU";
		// ParseObject.registerSubclass(User_Info.class);
		//NEW SDK configuration
		Parse.initialize(new Parse.Configuration.Builder(this.getApplicationContext())
				.applicationId(ApplicationId)
				.server("https://equidad.herokuapp.com/parse/")

				.build()
		);
		location = getLatLong(WayGroup.this);
		Utils.makeDir();
		// userCountry();
		// TODO Auto-generated method stub
		final String android_id = "user"
				+ Secure.getString(getContentResolver(), Secure.ANDROID_ID);

		ParsePush.subscribeInBackground(android_id, new SaveCallback() {

			@Override
			public void done(ParseException arg0) {
				// TODO Auto-generated method stub
				if (arg0 == null) {
					Log.d("com.parse.push",
							"successfully subscribed to the broadcast channel." + android_id );
				} else {
					Log.e("com.parse.push", "failed to subscribe for push",
							arg0);
				}
			}
		});

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheOnDisc(true).cacheInMemory(true).considerExifParams(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.defaultDisplayImageOptions(defaultOptions)
				.memoryCache(new WeakMemoryCache())
				.discCacheSize(100 * 1024 * 1024).build();

		ImageLoader.getInstance().init(config);
	}

	public static String getLatLong(Context context) {
		GPSTracker gps = new GPSTracker(context);

		// check if GPS enabled
		if (gps.canGetLocation()) {
			System.out.println("GPS Enabled Now");
			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();
			Geocoder geocoder;
			List<Address> addresses;
			geocoder = new Geocoder(context, Locale.getDefault());

			try {
				System.out.println("GPS Enabled Now" + latitude);
				addresses = geocoder.getFromLocation(latitude, longitude, 1);
				if (addresses != null && !addresses.isEmpty()) {
					String address = addresses.get(0).getAddressLine(0); // If
																			// any
																			// additional
																			// address
																			// line
																			// present
																			// than
																			// only,
																			// check
																			// with
																			// max
																			// available
																			// address
																			// lines
																			// by
																			// getMaxAddressLineIndex()
					String city = addresses.get(0).getLocality();

					String state = addresses.get(0).getAdminArea();
					String country = addresses.get(0).getCountryName();
					// String postalCode = addresses.get(0).getPostalCode();
					String knownName = addresses.get(0).getFeatureName();
					location = address + "," + city + "," + state + ","
							+ country;
					System.out.println("New Location " + location);
					Editor editor1 = preference.edit();
					editor1.putString("MyLocation", location);
					editor1.commit();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // Here 1 represent max
				// \n is for new line
				// Toast.makeText(getApplicationContext(),
				// "Your Location is - \nLat: " + latitude + "\nLong: " +
				// longitude, Toast.LENGTH_LONG).show();

		} else {

			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			// gps.showSettingsAlert();
		}
		return location;
	}

	

}
