package com.tucan.equidadarl;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.meteosoft.waygroup.database.ImageInfoUpdate;
import com.meteosoft.waygroup.database.ImageInfoUpdateHelper;

public class LocalDataActivity extends Activity {
	Local_CustomList localCustomList;
	private int index;
	ImageInfoUpdateHelper imgHelper;
	ListView list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_local_data);

		ImageButton home_btn = (ImageButton) findViewById(R.id.homeLocal);
		ImageButton webLink1 = (ImageButton) findViewById(R.id.webLinkLocal);
		webLink1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LocalDataActivity.this,
						WebPageActivity.class);
				startActivity(intent);
			}

		});
		home_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}

		});

		list = (ListView) findViewById(R.id.listLocal);
		imgHelper = new ImageInfoUpdateHelper();

		new AddStringTask().execute();

		// index = list.getFirstVisiblePosition();
		ImageButton downBtn = (ImageButton) findViewById(R.id.downBtn1);
		downBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				index = list.getFirstVisiblePosition();
				index = index + 1;
				list.setSelection(index);
			}
		});

	}

	class AddStringTask extends AsyncTask<Void, String, Void> {
		List<ImageInfoUpdate> user;
		List<ImageInfoUpdate> filteredList = new ArrayList<ImageInfoUpdate>();
		List<Bitmap> bitmaps = new ArrayList<Bitmap>();

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			user = imgHelper.getImageInfo(getApplicationContext(), "2", true);
			SharedPreferences preference = getSharedPreferences(
					"MyPREFERENCES", Context.MODE_PRIVATE);
			String userName = preference.getString("username2", " ");
			if (user != null && user.size() > 0) {
				for (ImageInfoUpdate imageInfo : user) {
					if ((imageInfo.userName).equals(userName)) {
						filteredList.add(imageInfo);
					}
				}
			}

			/*
			 * for (ImageInfo imageInfo : user) { // imageInfo.getImgPath();
			 * 
			 * Bitmap myBitmap; Bitmap myBitmap1; myBitmap =
			 * BitmapFactory.decodeFile(imageInfo.getImgPath()); ExifInterface
			 * exif = null; try { exif = new
			 * ExifInterface(imageInfo.getImgPath()); } catch (IOException e) {
			 * // TODO Auto-generated catch block e.printStackTrace(); } int
			 * orientation = exif.getAttributeInt(
			 * ExifInterface.TAG_ORIENTATION, 1); Log.d("EXIF", "Exif: " +
			 * orientation); Matrix matrix = new Matrix(); if (orientation == 6)
			 * { matrix.postRotate(90); } else if (orientation == 3) {
			 * matrix.postRotate(180); } else if (orientation == 8) {
			 * matrix.postRotate(270); } myBitmap1 = Bitmap
			 * .createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
			 * myBitmap.getHeight(), matrix, true); Log.v("Bitmap", " " +
			 * myBitmap); Log.v("Bitmap1", " " + myBitmap1);
			 * 
			 * bitmaps.add(myBitmap1);
			 * 
			 * // listImage.setImageBitmap(myBitmap); }
			 */
			return (null);

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (filteredList.isEmpty() == true) {
				list.setVisibility(View.GONE);
				TextView noData = (TextView) findViewById(R.id.noData);
				noData.setVisibility(View.VISIBLE);
			} else {
				list.setVisibility(View.VISIBLE);
				localCustomList = new Local_CustomList(LocalDataActivity.this,
						filteredList);
				list.setAdapter(localCustomList);
			}
		}

	}
}
