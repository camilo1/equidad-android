package com.tucan.equidadarl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meteosoft.waygroup.database.ImageInfoUpdate;
import com.meteosoft.waygroup.database.ImageInfoUpdateHelper;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class ReportAndSeeActivity extends Activity {
	String owner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report_and_see);
		Intent intent1 = getIntent();
		final String userName = intent1.getStringExtra("UserName");
		final String userId = intent1.getStringExtra("userType");
		owner = intent1.getStringExtra("owner");
		LinearLayout reportLayout = (LinearLayout) findViewById(R.id.reportLayout);

		ImageButton cameraBtn = (ImageButton) findViewById(R.id.uploadImage);
		ImageButton viewBtn = (ImageButton) findViewById(R.id.viewBtn);

		cameraBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ReportAndSeeActivity.this,
						SelectImageActivity.class);
				intent.putExtra("UserName", userName);
				intent.putExtra("userType", userId);
				intent.putExtra("owner", owner);
				startActivity(intent);
			}
		});
		viewBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ReportAndSeeActivity.this,
						ReportActivity.class);
				intent.putExtra("UserName", userName);
				intent.putExtra("owner", owner);
				startActivity(intent);
			}

		});
		TextView bck = (TextView) findViewById(R.id.bckBtn4);
		bck.setText("<");
		findViewById(R.id.bckBtn4).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni != null) {
			new UploadRestDataToParseAsyncTask().execute();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	class UploadRestDataToParseAsyncTask extends AsyncTask<Void, String, Void> {
		List<ImageInfoUpdate> imageInfoList;

		@Override
		protected Void doInBackground(Void... params) {
			imageInfoList = new ImageInfoUpdateHelper().getImageInfo(
					getApplicationContext(), "1", false);
			if (imageInfoList != null && imageInfoList.size() > 0) {
				for (int index = 0; index < imageInfoList.size(); index++) {
					uploadDataToParse(imageInfoList.get(index));
				}
			}
			return (null);
		}

		@Override
		protected void onPostExecute(Void result) {

		}

		public void uploadDataToParse(final ImageInfoUpdate imageInfo) {
			ParseFile file = new ParseFile("test.png",
					getByteArray(imageInfo.getImgPath()));
			ParseObject imageData = new ParseObject("ImageData");
			imageData.put("description", imageInfo.getDescription());
			imageData.put("title", imageInfo.getTitle());
			imageData.put("date", imageInfo.getDateTime());
			imageData.put("ownername", owner);
			imageData.put("location", imageInfo.getLocation());
			imageData.put("username", imageInfo.getuserName());
			imageData.put("usertype", Integer.parseInt(imageInfo.getUserType()));
			imageData.put("image", file);
			imageData.saveInBackground(new SaveCallback() {

				@Override
				public void done(ParseException e) {
					if (e == null) {
						new ImageInfoUpdateHelper().deleteImageFromLocal(
								ReportAndSeeActivity.this, imageInfo);
					}
				}
			});
		}

		public byte[] getByteArray(String imgPath) {

			Bitmap scaledBitmap = null;

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			Bitmap bmp = BitmapFactory.decodeFile(imgPath, options);

			float actualHeight = options.outHeight;
			float actualWidth = options.outWidth;

			// max Height and width values of the compressed image is taken as
			// 816x612

			float maxHeight = 816.0f;
			float maxWidth = 612.0f;
			float imgRatio = actualWidth / actualHeight;
			float maxRatio = maxWidth / maxHeight;

			// width and height values are set maintaining the aspect ratio of
			// the image

			if (actualHeight > maxHeight || actualWidth > maxWidth) {
				if (imgRatio < maxRatio) {
					imgRatio = maxHeight / actualHeight;
					actualWidth = (float) (imgRatio * actualWidth);
					actualHeight = (float) maxHeight;
				} else if (imgRatio > maxRatio) {
					imgRatio = maxWidth / actualWidth;
					actualHeight = (float) (imgRatio * actualHeight);
					actualWidth = (float) maxWidth;
				} else {
					actualHeight = (float) maxHeight;
					actualWidth = (float) maxWidth;

				}
			}

			options.inSampleSize = calculateInSampleSize(options, actualWidth,
					actualHeight);

			// inJustDecodeBounds set to false to load the actual bitmap
			options.inJustDecodeBounds = false;

			// this options allow android to claim the bitmap memory if it runs
			// low on memory
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inTempStorage = new byte[16 * 1024];

			try {
				// load the bitmap from its path
				bmp = BitmapFactory.decodeFile(imgPath, options);
			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();

			}

			try {
				// if((int)actualWidth>0 && (int)actualHeight>0){
				scaledBitmap = Bitmap.createBitmap((int) actualWidth,
						(int) actualHeight, Bitmap.Config.ARGB_8888);
				// }

			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			float ratioX = actualWidth / (float) options.outWidth;
			float ratioY = actualHeight / (float) options.outHeight;
			float middleX = actualWidth / 2.0f;
			float middleY = actualHeight / 2.0f;

			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
					middleY - bmp.getHeight() / 2, new Paint(
							Paint.FILTER_BITMAP_FLAG));

			// check the rotation of the image and display it properly
			ExifInterface exif;
			try {
				exif = new ExifInterface(imgPath);

				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 0);
				Log.d("EXIF", "Exif: " + orientation);
				Matrix matrix = new Matrix();
				if (orientation == 6) {
					matrix.postRotate(90);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 3) {
					matrix.postRotate(180);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 8) {
					matrix.postRotate(270);
					Log.d("EXIF", "Exif: " + orientation);
				}
				scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
						scaledBitmap.getWidth(), scaledBitmap.getHeight(),
						matrix, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
			return stream.toByteArray();
		}

		private int calculateInSampleSize(Options options, float reqWidth,
				float reqHeight) {
			// TODO Auto-generated method stub
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				final int heightRatio = Math.round((float) height
						/ (float) reqHeight);
				final int widthRatio = Math.round((float) width
						/ (float) reqWidth);
				inSampleSize = heightRatio < widthRatio ? heightRatio
						: widthRatio;
			}
			final float totalPixels = width * height;
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;
			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}

			return inSampleSize;
		}
	}

}
