package com.tucan.equidadarl;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.Toast;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

//import com.world.AndroidBadger.ShortcutBadgeException;
//import com.world.AndroidBadger.ShortcutBadger;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import me.leolin.shortcutbadger.ShortcutBadger;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		SharedPreferences preference = getSharedPreferences("MyPREFERENCES",
				Context.MODE_PRIVATE);

		Editor editor = preference.edit();
		editor.putInt("badgeNo", 0);
		editor.commit();
		NotificationManager notifManager = (NotificationManager) getApplicationContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancelAll();
		ShortcutBadger.applyCount(getApplicationContext(),
				preference.getInt("badgeNo", -1));
		/*try {
			Editor editor = preference.edit();
			editor.putInt("badgeNo", 0);
			editor.commit();
			NotificationManager notifManager = (NotificationManager) getApplicationContext()
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notifManager.cancelAll();
			ShortcutBadger.applyCount(getApplicationContext(),
					preference.getInt("badgeNo", -1));

		} catch (ShortcutBadgeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		if (((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)) {
			finish();
			return;
		}
		LinearLayout layout1 = (LinearLayout) findViewById(R.id.layout1);
		ImageButton webLink = (ImageButton) findViewById(R.id.webLink);
		ImageButton phone_btn = (ImageButton) findViewById(R.id.phone_btn);
		phone_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
				builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						//Do Something
						Intent intent = new Intent(Intent.ACTION_DIAL);
						intent.setData(Uri.parse("tel:0317460392"));
						startActivity(intent);

					}
				});
				builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						//do something
					}
				});

				builder.setTitle("ADVERTENCIA");
				builder.setMessage("Esta llamada tendrá un costo de acuerdo a las tarifas vigentes de tu operador, de lo contrario comunícate con el # 324");

				AlertDialog alertDialog = builder.create();
				alertDialog.show();

			}

		});
		webLink.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SplashActivity.this,
						WebPageActivity.class);
				startActivity(intent);
			}
		});
		layout1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SplashActivity.this,
						UserSelectActivity.class);
				startActivity(intent);
				return false;

			}

		});

	}
}
