package com.tucan.equidadarl;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class UserSelectActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_select);
		ImageButton user1 = (ImageButton) findViewById(R.id.user1);
		ImageButton user2 = (ImageButton) findViewById(R.id.user2);
		user1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(UserSelectActivity.this,
						LoginUser1Activity.class);
				intent.putExtra("UserId", "1");
				startActivity(intent);
			}
		});
		user2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(UserSelectActivity.this,
						LoginUser1Activity.class);
				intent.putExtra("UserId", "2");
				startActivity(intent);
			}
		});
		TextView bck = (TextView) findViewById(R.id.bckBtn1);
		bck.setText("<");
		findViewById(R.id.bckBtn1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
}