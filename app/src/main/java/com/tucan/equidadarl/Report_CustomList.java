package com.tucan.equidadarl;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.ParseObject;

public class Report_CustomList extends ArrayAdapter<ParseObject> {
	ParseObject pdata = null;
	private final Activity context;
	List<ParseObject> allImagedata;
	public ArrayList<ParseObject> filtered = new ArrayList<ParseObject>();
	public boolean isFiltered;
	// ImageLoader il;
	ImageLoader imageLoader = ImageLoader.getInstance();

	public Report_CustomList(Activity context, int resource,
			int textViewResourceId, List<ParseObject> allImagedata) {
		super(context, resource, textViewResourceId, allImagedata);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.allImagedata = allImagedata;
		// il = new ImageLoader(context.getApplicationContext());
	}

	@SuppressLint("NewApi")
	public View getView(int position, View view, final ViewGroup parent) {
		// Options decodingOptions = new Options();
		final DisplayImageOptions options = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisc(true)
				.resetViewBeforeLoading(true).considerExifParams(true)
				.showImageForEmptyUri(R.drawable.place_holder)
				.showImageOnFail(R.drawable.place_holder)
				.showImageOnLoading(R.drawable.place_holder)
				// .decodingOptions(decodingOptions)
				// .imageScaleType(ImageScaleType.NONE)
				.build();
		View v = view;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.list_row, parent, false);

		}

		Display display = context.getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		Log.i("Display Width", "" + width);
		int height = display.getHeight();
		Log.v("height", " " + height);

		height = (int) (0.83 * display.getHeight());
		LinearLayout image_row = (LinearLayout) v.findViewById(R.id.image_row);
		LayoutParams params = image_row.getLayoutParams();
		if (params == null) {
			params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
		} else {
			params.height = height;
		}

		image_row.setLayoutParams(params);

		// image_Layout.setMinimumHeight(height_layout);

		if (!isFiltered)
			pdata = allImagedata.get(position);
		else if (filtered.size() > 0)
			pdata = filtered.get(position);
		ImageButton moreBtnClick=(ImageButton)v.findViewById(R.id.moreBtnClick); 
		moreBtnClick.setTag(pdata);
		final ImageView listImage = (ImageView) v.findViewById(R.id.listImage);
		final TextView DescriptionText = (TextView) v
				.findViewById(R.id.DescriptionText);
		TextView titleTextView = (TextView) v.findViewById(R.id.titleText);
		TextView LocationText = (TextView) v.findViewById(R.id.LocationText);
		TextView TimeText = (TextView) v.findViewById(R.id.TimeText);
		TextView DateText = (TextView) v.findViewById(R.id.DateText);
		DescriptionText.setText(pdata.getString("description"));
		LocationText.setText(pdata.getString("location"));
		java.util.Date date = new Date(position);
		date = pdata.getDate("date");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		String month = month_date.format(cal.getTime());
		int Date = cal.get(Calendar.DAY_OF_MONTH);
		SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		String time = sdfs.format(date).toString();
		String dDate = month + " " + Date + " " + year;
		TimeText.setText(time);
		DateText.setText(dDate);
		final String imgUrl = pdata.getParseFile("image").getUrl();
		Log.v("imagePath", imgUrl);
		// final Bitmap bm = il.getBitmap(imgUrl);
		listImage.setVisibility(View.VISIBLE);
		imageLoader.displayImage(imgUrl, listImage, options);
		String titleText = pdata.getString("title");
		if (titleText != null && !titleText.equals("")) {
			titleTextView.setVisibility(View.VISIBLE);
			titleTextView.setText(titleText);
		} else {
			titleTextView.setVisibility(View.GONE);
		}

		// BitmapDrawable ob = new BitmapDrawable(il.getBitmap(imgUrl));
		// listImage.setBackgroundDrawable(ob);
		// il.DisplayImage(imgUrl, listImage);
		// ImageLoader.getInstance().displayImage(imgUrl, listImage,
		// animateFirstListener);

		/*
		 * ParseFile fileObject = pdata.getParseFile("image");
		 * fileObject.getDataInBackground(new GetDataCallback() {
		 * 
		 * @Override public void done(byte[] data, com.parse.ParseException e) {
		 * // TODO Auto-generated method stub if (e == null) { Log.d("test",
		 * "We've got data in data."); // Decode the Byte[] into // Bitmap
		 * Bitmap bmp = BitmapFactory.decodeByteArray(data, 0,data.length); //
		 * Get the ImageView from main.xml //ImageView image = (ImageView)
		 * findViewById(R.id.ad1);
		 * 
		 * // Set the Bitmap into the // ImageView
		 * listImage.setImageBitmap(bmp); // Close progress dialog
		 * 
		 * } else { Log.d("test", "There was a problem downloading the data.");
		 * } } });
		 */

		DescriptionText.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().getParent()
						.requestDisallowInterceptTouchEvent(true);
				DescriptionText
						.setMovementMethod(new ScrollingMovementMethod());
				return false;
			}
		});
		listImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// ImageLoader il;
				// il = new ImageLoader(context);
				final PopupWindow pwindo;
				ImageButton cross_btn;
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.image_show, parent,
						false);

				pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT, true);
				pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
				ImageView showImage = (ImageView) layout
						.findViewById(R.id.imageShow);
				imageLoader.displayImage(imgUrl, showImage);
				cross_btn = (ImageButton) layout.findViewById(R.id.cross_btn);
				// String imgUrl = pdata.getParseFile("imageFile").getUrl();
				// showImage.setImageBitmap(bm);
				// il.DisplayImage(imgUrl, showImage);
				cross_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pwindo.dismiss();
					}
				});
			}
		});

		moreBtnClick.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final PopupWindow pwindo;

				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.description_show,
						parent, false);
				ParseObject  user = (ParseObject) v.getTag();
				pwindo = new PopupWindow(layout, LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT, true);
				pwindo.showAtLocation(parent, Gravity.CENTER, 0, 0);
				ImageView showImage = (ImageView) layout
						.findViewById(R.id.imageShow);
				ImageButton cross_btn = (ImageButton) layout
						.findViewById(R.id.cross_btn);
				TextView title = (TextView) layout
						.findViewById(R.id.titleTextView);
				TextView desc = (TextView) layout
						.findViewById(R.id.description);
				desc.setText(user.getString("description"));
				if (user.getString("title") != null && !user.getString("title").equals("")) {
					title.setVisibility(View.VISIBLE);
					title.setText(user.getString("title"));
				} else {
					title.setVisibility(View.GONE);
				}
				final String imgUrl = user.getParseFile("image").getUrl();
				imageLoader.displayImage(imgUrl, showImage);
				cross_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pwindo.dismiss();
					}
				});

			}
		});

		return v;
	}

	public int getCount() {
		if (filtered.isEmpty() && !isFiltered)
			return allImagedata.size();
		else
			return filtered.size();
	}

}
