package com.tucan.equidadarl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

public class NotificationRecieverDialogActivity extends Activity {

	String message = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_alarm_reciever_dialog);
		Intent intent = getIntent();
		message = intent.getStringExtra("notificationMessage");
		if (message != null && message.compareTo("") != 0
				&& message.compareTo(" ") != 0) {
			Log.v("senderName ", message);
			showAlertDialog(message);
		} else {
			Log.v("senderName", "message null");
			finish();
		}
	}

	public void showAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("WayGroup");
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				// do something when the OK button is clicked
				NotificationRecieverDialogActivity.this.finish();
				// UnityPlayer.UnitySendMessage("Main Camera",
				// "OnPushNotification", "");
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		TextView messageText = (TextView) alert
				.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		int textViewId = alert.getContext().getResources()
				.getIdentifier("android:id/alertTitle", null, null);
		TextView tv = (TextView) alert.findViewById(textViewId);
		if (tv != null) {
			tv.setTextColor(getResources().getColor(android.R.color.white));
			tv.setGravity(Gravity.CENTER);
		}

	}
}
