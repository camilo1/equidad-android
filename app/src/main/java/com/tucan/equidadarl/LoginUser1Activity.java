package com.tucan.equidadarl;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class LoginUser1Activity extends Activity {

	ProgressDialog progress;
	String username1 = "username1";
	String pwd1 = "pwd1";
	String username2 = "username2";
	String pwd2 = "pwd2";
	SharedPreferences preference;
	String ownerName;
	String userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login_user1);
		Intent intent = getIntent();
		userId = intent.getStringExtra("UserId");
		preference = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
		final EditText userName = (EditText) findViewById(R.id.userName1);
		final EditText password = (EditText) findViewById(R.id.Password1);
		ImageView userSelect = (ImageView) findViewById(R.id.userSelect);
		LinearLayout Layout = (LinearLayout) findViewById(R.id.Layout1);
		if (userId.equals("1") == true) {
			userName.setText(preference.getString(username1, ""));
			password.setText(preference.getString(pwd1, ""));
		} else if (userId.equals("2") == true) {
			userSelect.setImageResource(R.drawable.u2);
			userName.setText(preference.getString(username2, ""));
			password.setText(preference.getString(pwd2, ""));
		}
		TextView bck = (TextView) findViewById(R.id.bckBtn2);
		bck.setText("<");
		findViewById(R.id.bckBtn2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		final ImageButton loginBtn1 = (ImageButton) findViewById(R.id.loginBtn1);

		loginBtn1.setOnClickListener(new OnClickListener() {

			@TargetApi(Build.VERSION_CODES.GINGERBREAD)
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

				final NetworkInfo ni = cm.getActiveNetworkInfo();
				// loginBtn1.setEnabled(false);

				final String UserName = userName.getText().toString();
				final String Password = password.getText().toString();
				if (Password.isEmpty() == true) {
					Toast.makeText(getBaseContext(), "ingrese su contraseña",
							Toast.LENGTH_SHORT).show();
				} else if (UserName.isEmpty() == true) {
					Toast.makeText(getBaseContext(),
							"ingrese su nombre de usuario", Toast.LENGTH_SHORT)
							.show();
				}

				else {

					Editor editor1 = preference.edit();
					if (userId.equals("1") == true) {
						editor1.putString(username1, userName.getText()
								.toString());
						editor1.putString(pwd1, password.getText().toString());
					} else if (userId.equals("2") == true) {
						editor1.putString(username2, userName.getText()
								.toString());
						editor1.putString(pwd2, password.getText().toString());
					}
					editor1.commit();
					Log.v("usern", preference.getString(UserName, ""));
					progress = new ProgressDialog(LoginUser1Activity.this);
					progress.setMessage("Cargando");
					progress.show();
					progress.setCancelable(false);

					ParseUser.logInInBackground(UserName, Password,
							new LogInCallback() {

								@Override
								public void done(ParseUser user,
										ParseException e) {
									// TODO Auto-generated method stub
									progress.dismiss();
									loginBtn1.setEnabled(true);
									if (e == null) {

										if (user.getString("usertype").equals(
												userId) == false) {
											Intent dialogIntent = new Intent(
													LoginUser1Activity.this,
													AlarmRecieverDialogActivity.class);
											dialogIntent
													.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
											startActivity(dialogIntent);
										} else {

											if (userId.equals("1") == true) {
												ownerName = user
														.getString("owner");
												Intent intent = new Intent(
														LoginUser1Activity.this,
														ReportAndSeeActivity.class);
												intent.putExtra("UserName",
														UserName);
												intent.putExtra("userType",
														userId);
												intent.putExtra("owner",
														ownerName);
												startActivity(intent);
											} else if (userId.equals("2") == true) {
												ownerName = user
														.getString("owner");
												Intent intent = new Intent(
														LoginUser1Activity.this,
														ReportAndSeeUser2Activity.class);

												intent.putExtra("UserName",
														UserName);
												intent.putExtra("userType",
														userId);
												intent.putExtra("owner",
														ownerName);
												startActivity(intent);
											}
											sendDeviceId();
										}
									} else {
										if (ni == null) {
											// There are no active networks.
											Toast.makeText(getBaseContext(),
													"Red no disponible",
													Toast.LENGTH_SHORT).show();
										} else {
											Intent dialogIntent = new Intent(
													LoginUser1Activity.this,
													AlarmRecieverDialogActivity.class);
											dialogIntent
													.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
											startActivity(dialogIntent);
										}
									}
								}

							});
					/*
					 * ParseQuery<ParseObject> query =
					 * ParseQuery.getQuery("User");
					 * query.whereEqualTo("username", UserName);
					 * query.whereEqualTo("password", Password);
					 * query.whereEqualTo("usertype", userId);
					 * query.getFirstInBackground(new GetCallback<ParseObject>()
					 * {
					 * 
					 * @Override public void done(ParseObject object,
					 * ParseException e) { // TODO Auto-generated method stub
					 * progress.dismiss(); loginBtn1.setEnabled(true); if (e ==
					 * null) {
					 * 
					 * if (userId.equals("1") == true) { Intent intent = new
					 * Intent( LoginUser1Activity.this,
					 * ReportAndSeeActivity.class); intent.putExtra("UserName",
					 * UserName); //intent.putExtra("userType", userId);
					 * startActivity(intent); } else if (userId.equals("2") ==
					 * true) { Intent intent = new Intent(
					 * LoginUser1Activity.this, LoginUser2Activity.class);
					 * 
					 * intent.putExtra("UserName", UserName);
					 * //intent.putExtra("userType", userId);
					 * startActivity(intent); } } else {
					 * 
					 * Toast.makeText(getBaseContext(),
					 * "UserId or Password is incorrect",
					 * Toast.LENGTH_SHORT).show();
					 * 
					 * }
					 * 
					 * }
					 * 
					 * });
					 */
				}
			}
		});

	}

	private void sendDeviceId() {
		// TODO Auto-generated method stub
		final String android_id = "user"
				+ Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DeviceIDs");
		query.whereEqualTo("deviceID", android_id);
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				// TODO Auto-generated method stub
				if (object == null) {
					ParseObject deviceData = new ParseObject("DeviceIDs");

					deviceData.put("owner", ownerName);

					deviceData.put("userType", userId);
					deviceData.put("deviceID", android_id);
					deviceData.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							if (e == null) {
								Log.v("Data Saved", "data uploaded");

							} else {

								e.printStackTrace();
							}

						}
					});
				} else {
					object.put("owner", ownerName);
					object.put("userType", userId);
					object.saveInBackground();
				}
			}
		});

	}
}