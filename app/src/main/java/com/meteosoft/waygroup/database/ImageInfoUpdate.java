package com.meteosoft.waygroup.database;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ImageInfoUpdate")
public class ImageInfoUpdate {
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	public String userName;
	@DatabaseField
	public String title;
	@DatabaseField
	public String Description;
	@DatabaseField
	public Date DateTime;
	@DatabaseField
	public String Location;
	@DatabaseField
	public String ImgPath;

	@DatabaseField
	public String userType;
	@DatabaseField
	public boolean isUploaded;

	public ImageInfoUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageInfoUpdate(String userName, String title, String Description,
			Date date, String Location, String ImgPath, String userType,
			boolean isUploaded) {
		super();
		this.userName = userName;
		this.title = title;
		this.Description = Description;
		this.DateTime = date;
		this.Location = Location;
		this.ImgPath = ImgPath;
		this.userType = userType;
		this.isUploaded = isUploaded;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public Date getDateTime() {
		return DateTime;
	}

	public void setDateTime(Date DateTime) {
		this.DateTime = DateTime;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String Location) {
		this.Location = Location;
	}

	public String getImgPath() {
		return ImgPath;
	}

	public void setImgPath(String ImgPath) {
		this.ImgPath = ImgPath;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}

}
