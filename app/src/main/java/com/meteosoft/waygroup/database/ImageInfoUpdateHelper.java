package com.meteosoft.waygroup.database;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class ImageInfoUpdateHelper {
	public void setRefreshDao(Context context, ImageInfoUpdate img)
			throws SQLException {
		DataBaseHelper helperDB = new DataBaseHelper(context);
		Dao<ImageInfoUpdate, String> ImageDao = helperDB.getImageInfoUpdateDao();
		try {
			ImageDao.refresh(img);
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		helperDB.close();
	}

	public void addData(Context context, ImageInfoUpdate img) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<ImageInfoUpdate, String> dao;
		try {

			dao = dbHelper.getImageInfoUpdateDao();
			dao.create(img);
			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<ImageInfoUpdate> getImageInfo(Context context, String userType,
			boolean isUploaded) throws SQLException {
		DataBaseHelper helperDB = new DataBaseHelper(context);
		Dao<ImageInfoUpdate, String> Dao = helperDB.getImageInfoUpdateDao();
		List<ImageInfoUpdate> list = null;
		try {
			QueryBuilder<ImageInfoUpdate, String> queryBuilder = Dao.queryBuilder();
			Where<ImageInfoUpdate, String> where = queryBuilder.where();
			where.eq("userType", userType);
			where.and();
			where.eq("isUploaded", isUploaded);
			list = queryBuilder.query();
			Collections.reverse(list);

		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		helperDB.close();
		return list;
	}

	public void updateData(Context context, ImageInfoUpdate image) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<ImageInfoUpdate, String> dao;
		try {
			dao = dbHelper.getImageInfoUpdateDao();
			dao.update(image);
			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteImageFromLocal(Context context, ImageInfoUpdate image) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<ImageInfoUpdate, String> dao;
		try {
			dao = dbHelper.getImageInfoUpdateDao();
			dao.delete(image);
			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
