package com.meteosoft.waygroup.database;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ImageInfo")
public class ImageInfo {
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	public String userName;
	@DatabaseField
	public String title;
	@DatabaseField
	public String Description;
	@DatabaseField
	public Date DateTime;
	@DatabaseField
	public String Location;
	@DatabaseField
	public String ImgPath;


	public ImageInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageInfo(String userName, String title, String Description,
			Date date, String Location, String ImgPath) {
		super();
		this.userName = userName;
		this.title = title;
		this.Description = Description;
		this.DateTime = date;
		this.Location = Location;
		this.ImgPath = ImgPath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public Date getDateTime() {
		return DateTime;
	}

	public void setDateTime(Date DateTime) {
		this.DateTime = DateTime;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String Location) {
		this.Location = Location;
	}

	public String getImgPath() {
		return ImgPath;
	}

	public void setImgPath(String ImgPath) {
		this.ImgPath = ImgPath;
	}
}
