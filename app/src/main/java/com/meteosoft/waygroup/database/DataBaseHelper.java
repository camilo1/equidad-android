package com.meteosoft.waygroup.database;

import java.util.List;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.meteosoft.infrastructure.Utils;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {
	private static final String DATABASE_NAME = "ImgInfo.db";
	private static final int DATABASE_VERSION = 3;
	private Dao<ImageInfo, String> ImageDao = null;
	private Dao<ImageInfoUpdate, String> imageInfoUpdateDao = null;

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource) {
		// TODO Auto-generated method stub

		try {
			TableUtils.createTable(connectionSource, ImageInfoUpdate.class);
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource, int oldVer, int newVer) {
		// TODO Auto-generated method stub
		try {
			List<ImageInfo> imageInfoList = getImageInfoDao().queryForAll();
			TableUtils.dropTable(connectionSource, ImageInfo.class, true);
			onCreate(sqliteDatabase, connectionSource);
			for (ImageInfo imageInfo : imageInfoList) {
				ImageInfoUpdate img;
				if (oldVer == 2 && newVer == 3) {

					img = new ImageInfoUpdate(imageInfo.getuserName(),
							imageInfo.getTitle(), imageInfo.getDescription(),
							imageInfo.getDateTime(), imageInfo.getLocation(),
							imageInfo.getImgPath(), "2", true);

				} else {
					img = new ImageInfoUpdate(imageInfo.getuserName(), "",
							imageInfo.getDescription(),
							imageInfo.getDateTime(), imageInfo.getLocation(),
							imageInfo.getImgPath(), "2", true);
				}
				getImageInfoUpdateDao().create(img);
			}
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public Dao<ImageInfo, String> getImageInfoDao() throws SQLException {
		if (ImageDao == null) {
			try {
				ImageDao = BaseDaoImpl.createDao(getConnectionSource(),
						ImageInfo.class);
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ImageDao;
	}

	public Dao<ImageInfoUpdate, String> getImageInfoUpdateDao()
			throws SQLException {
		if (imageInfoUpdateDao == null) {
			try {
				imageInfoUpdateDao = BaseDaoImpl.createDao(
						getConnectionSource(), ImageInfoUpdate.class);
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return imageInfoUpdateDao;
	}

}
